<?php

/*
 * @file
 * Configuration and UI forms for fieldaccess module.
 */

function fieldaccess_settings_admin_form($form, &$form_state) {
  $form = array();
  $form['directions'] = array(
    '#type' => 'item',
    '#title' => t('Supported entity and field types'),
    '#markup' => t('<em>Use at your own risk, do not touch unless you know what you are doing. </em>Implementations of Field Access can (should) limit themselves to a set of "known good" combinations of entity types and field types. You can override these on this page. <em>Use at your own risk, do not touch unless you know what you are doing.</em>'),
  );
  $form['settings'] = array();
  $entities_infos = entity_get_info();
  $fields_infos = field_info_field_types();
  $implements = fieldaccess_implements_info();
  $access_types = _fieldaccess_access_types_description();
  foreach ($implements as $implement_name => $implement) {
    $module_name = $implement['module'];
    $defaults = fieldaccess_get_supported_types($implement_name);
    if (!isset($form['settings'][$implement['type']])) {
      $form['settings'][$implement['type']] = array(
        '#type' => 'fieldset',
        '#title' => $access_types[$implement['type']]['title'],
        '#description' => $access_types[$implement['type']]['description'],
      );
    }
    if (!isset($form['settings'][$implement['type']][$module_name])) {
      $module_infos = system_get_info('module', $module_name);
      $form['settings'][$implement['type']][$module_name] = array(
        '#type' => 'fieldset',
        '#title' => $module_infos['name'],
        '#description' => $module_infos['description'],
      );
    }
    $form['settings'][$implement['type']][$module_name][$implement_name] = array(
      '#type' => 'fieldset',
      '#title' => $implement['label'],
      '#tree' => TRUE,
    );
    $types_options = array();
    foreach ($entities_infos as $entity_name => $entity_infos) {
      if ($entity_infos['fieldable']) {
        $options = array();
        foreach ($fields_infos as $type_name => $type_infos) {
          $options[$type_name] = $type_infos['label'];
        }
        $types_options[$entity_name] = array(
          '#type' => 'fieldset',
          '#title' => $entity_infos['label'],
        );
        $types_options[$entity_name][$entity_name] = array(
          '#type' => 'checkboxes',
          '#options' => $options,
          '#attributes' => array('class' => array('container-inline')),
          '#default_value' => isset($defaults[$entity_name]) ? $defaults[$entity_name] : array(),
        );
      }
    }
    $form['settings'][$implement['type']][$module_name][$implement_name] += $types_options;
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function fieldaccess_settings_admin_form_submit($form, &$form_state) {
  $clean = $form_state;
  form_state_values_clean($clean);
  $settings = array();
  foreach ($clean['values'] as $implement => $entities_array) {
    foreach ($entities_array as $entity_name => $entity_array) {
      $settings[$implement][$entity_name] = current($entity_array);
    }
  }
  variable_set('fieldaccess_supported_types', $settings);
}

/*
 * Helper function. Returns access type descriptions,
 * used on forms.
 */

function _fieldaccess_access_types_description() {
  $access_types = array(
    'edit' => array(
      'title' => t('EDIT access'),
      'description' => t('Restrict the editing of the field.'),
    ),
    'view' => array(
      'title' => t('VIEW access'),
      'description' => t('Restrict the viewing of the field.'),
    ),
    'edit_override' => array(
      'title' => t('EDIT display override'),
      'description' => t('How to react when access is denied on edit.'),
    ),
    'view_override' => array(
      'title' => t('VIEW display override'),
      'description' => t('How to react when access is denied on view.'),
    ),
  );
  return $access_types;
}