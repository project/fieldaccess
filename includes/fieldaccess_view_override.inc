<?php

/*
 * @file
 * Callbacks function for view display override implementations
 */

/*
 * Show the rendered field instead of the edit widget.
 */

function fa_view_show_rendered_override($settings, $element, $context) {
  if ($element['#view_mode'] !== 'fa_view_override') {
    return field_view_field($context['entity_type'], $context['entity'], $context['field']['field_name'], 'fa_view_override');
  }
  return $element;
}

/*
 * Settings for rendering the field.
 */

function fa_view_show_rendered_configure($settings, &$form, &$form_state) {
  $elements = array(
    '#markup' => t('You can choose how to display the field by adjusting the display for the "View override" view mode after saving.'),
  );
  return $elements;
}

