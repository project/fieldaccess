<?php

/*
 * @file
 * Callbacks function for edit display override implementations
 */

/*
 * Disable the form element.
 */

function fa_edit_disable_override($settings, $element, $context) {
  $element['#disabled'] = TRUE;
  return $element;
}

/*
 * Show the rendered field instead of the edit widget.
 */

function fa_edit_show_rendered_override($settings, $element, $context) {
  if ($element['#view_mode'] !== 'fa_edit_override') {
    return field_view_field($context['entity_type'], $context['entity'], $context['field']['field_name'], 'fa_edit_override');
  }
  return $element;
}

/*
 * Settings for rendering the field.
 */

function fa_edit_show_rendered_configure($settings, &$form, &$form_state) {
  $elements = array(
    '#markup' => t('You can choose how to display the field by adjusting the display for the "View override" view mode after saving.'),
  );
  return $elements;
}

