<?php

/**
 * @file access.inc
 * Access callbacks for field access defined in fa_user.module.
 * 
 */

/**
 * Grant callback for fa_user_author implementation of Field Access API.
 * 
 */
function fa_user_author_grant($settings, $object, $account, $context) {
// User is author.
  if ($object->uid === $account->uid) {
    return $settings['author'] == 1 ? TRUE : FALSE;
  }
  return $settings['author'] == 1 ? FALSE : TRUE;
}

/**
 * Grant callback for fa_user_role implementation of Field Access API.
 *
 */
function fa_user_role_grant($settings, $object, $account, $context) {
  // Cleanup roles array.
//@todo Should be done at validation time.
  $roles = array();
  foreach ($settings['roles'] as $role => $value) {
    if ($value) {
      $roles[$role] = $value;
    }
  }

// Access based on roles has been chosen,
// but no roles have been selected.
// This makes no sense and can only be a misconfiguration.
// Deny access in doubt.
  if (empty($roles)) {
    return FALSE;
  }


  $intersect = array_intersect($account->roles, $roles);
  // User has none of the selected role.
  if (empty($intersect)) {
    return $settings['roles_reverse'] == 1 ? TRUE : FALSE;
  }
  // User has at least one of the role, check operator.
  if ($settings['roles_operator'] === 'or') {
    return $settings['roles_reverse'] == 1 ? FALSE : TRUE;
  }
  // Operator is AND and user has all roles.
  if (count($intersect) == count($roles)) {
    return $settings['roles_reverse'] == 1 ? FALSE : TRUE;
  }
  // Operator is AND and user does not have all roles.
  return $settings['roles_reverse'] == 1 ? TRUE : FALSE;
}

/**
 * Grant callback for fa_user_role_author implementation of Field Access API.
 *
 */
function fa_user_role_author_grant($settings, $object, $account, $context) {
  if ($settings['role_author_operator'] === 'and') {
    return (fa_user_author_grant($settings, $object, $account, $context) && fa_user_role_grant($settings, $object, $account, $context));
  }
  return (fa_user_author_grant($settings, $object, $account, $context) || fa_user_role_grant($settings, $object, $account, $context));
}
