<?php

/**
 * @file admin.inc
 * Configuration forms callbacks for access settings defined in fa_user.module.
 */

/**
 * Configuration callback for fa_user_author implementation of Field Access API.
 */
function fa_user_author_configure($settings, &$form, &$form_state) {
  $elements['author'] = array(
    '#title' => t('User is author'),
    '#type' => 'checkbox',
    '#default_value' => isset($settings['author']) ? $settings['author'] : FALSE,
    '#description' => t('Leaving this unchecked does effectively means "user is NOT author"'),
  );
  return $elements;
}

/**
 * Configuration callback for fa_user_role implementation of Field Access API.
 */
function fa_user_role_configure($settings, &$form, &$form_state) {
  // Use role names instead of rid.
  $options = user_roles();
  $options = array_combine($options, $options);
  $elements['roles'] = array(
    '#title' => t('Roles'),
    '#type' => 'checkboxes',
    '#options' => $options,
    '#multiple' => TRUE,
    '#default_value' => isset($settings['roles']) ? $settings['roles'] : array(),
  );
  $elements['roles_operator'] = array(
    '#title' => t('Roles operator'),
    '#type' => 'radios',
    '#options' => array(
      'or' => t('User only needs to have one of the ticked roles (OR).'),
      'and' => t('User needs to have all of the ticked roles (AND).'),
    ),
    '#default_value' => isset($settings['roles_operator']) ? $settings['roles_operator'] : 'or',
  );
  $elements['roles_reverse'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reverse condition : user must not have the ticked roles.'),
    '#default_value' => isset($settings['roles_reverse']) ? $settings['roles_reverse'] : FALSE,
  );
  return $elements;
}

/**
 * Configuration callback for fa_user_role_author implementation of Field Access API.
 */
function fa_user_role_author_configure($settings, &$form, &$form_state) {
  $authorship = fa_user_role_configure($settings, $form, $form_state);
  $roles = fa_user_author_configure($settings, $form, $form_state);
  $elements = array_merge($roles, $authorship);
  $elements['role_author_operator'] = array(
    '#title' => t('Main operator'),
    '#type' => 'radios',
    '#options' => array(
      'or' => t('Either authorship or roles settings needs to be satisfied (OR).'),
      'and' => t('Both authorship and roles settings need to be satisfied (AND).'),
    ),
    '#default_value' => isset($settings['role_author_operator']) ? $settings['role_author_operator'] : 'or',
  );
  return $elements;
}