
 Field Access API sub-module
-----------------
by slv_, salvador.molinamoreno@codeenigma.com

This module is an implementation of the Field Access API module. It allows
site-builders to set field permissions (view / edit) based on data from the user
viewing an entity.

 Available Settings
---------------------------------------
A new access can be implementing hook_fieldaccess_info().

At the time of writing this, field permissions can be set by:

  - Authoring information (whether the user is the author of the node being
    accessed).
  - User roles

 Roadmap
---------------------------------------

  - Permissions based on user data should allow configuration in an 'AND' / 'OR'
    fashion, allowing more than 1 single option. Right now that's done but
    only for the authoring information and user roles, not in a generic way.
  - Need to decide which User data should be available on the Field Settings
    form.
  - Need to decide whether to provide a hook for other modules to add additional
    non-standard user info (and how).