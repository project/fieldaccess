<?php

/**
 * @file
 * Documentation for fieldaccess API.
 */

/**
 * @addtogroup hooks
 * @{
 * 
 */
/*
 * Define a new access implementation.
 * 
 * Implementations can be of 4 types :
 * - view : grant or deny access to a field being viewed.
 * - edit : grant or deny access to a field being edited.
 * - view_override : change the rendering of a field whose access as been denied when viewing it.
 * - edit_override : change the rendering of a field whose access as been denied when editing it.
 *
 * Any implementation defined by this hook can be set as the active one
 * through field's admin UI. While only one active implementation can be active
 * at a time per type for a given field instance, modules can define an unlimited number
 * of implementations.
 *
 * @return array
 * Access implementation(s) array(s), keyed by implementation's
 * unique name.
 *
 * @todo Move implementations properties definition here
 * instead of function's body.
 *
 */

function hook_fieldaccess_info() {
  $implements = array();

  // A full 'view' type implementation.
  $implements['mymodule _unique_name'] = array(
    // Mandatory. Type of operation the access rules will apply to.
    'type' => 'view',
    // Mandatory. Label as it will appear on the select list in the field config admin UI.
    'label' => t("Access depending on user's shirt color"),
    // Optional. The types of field the access rules can be applied to.
    // Default to all types if not specified.
    'field_types' => array('text', 'file'),
    // Optional. The types of entities the access rules can be applied to.
    // Default to all types if not specified.
    'entity_types' => array('text', 'file'),
    // Optional. The name of the function responsible for returning the configuration form elements.
    // Defaults to the access name suffixed with "_configure" (would be "mymodule_unique_name_configure" here)
    'configure' => 'mymodule_unique_name_view_configure',
    // Optional. Name of an include file containing the configure callback above.
    'configure_file' => 'admin.inc',
    // Optional. Path to the include file, relative to the drupal root directory.
    // Defaults to the module's root directory.
    'configure_path' => drupal_get_path('module', 'example') . '/includes',
    //Optional. The name of the function responsible for eventually granting/denying access to a field.
    //Defaults to the access name suffixed with "_grant" (would be "mymodule_unique_name_grant" here)
    'grant' => 'mymodule_unique_name_view_grant',
    //Optional. Name of an include file containing the grant callback above.
    'grant_file' => 'example.inc',
    // Defaults to the module's root directory.
    //Optional. Path to the include file, relative to the drupal root directory.
    'grant_path' => drupal_get_path('module', 'mymodule') . '/includes',
  );

  // A minimal 'edit' type.
  $implements['mymodule_another_one'] = array(
    'type' => 'edit',
    'label' => t("Access depending on user's shirt color"),
    // The grant callback would be defaulted to mymodule_another_one_grant(),
    // the configure callback (if any) to mymodule_another_one_configure().
  );

  // A 'view_override' example.
  // Types 'view_override' and 'edit_override' have the same properties
  // than the 'edit' and 'view' ones, except they use an 'override' callback instead
  // of a 'grant' one.
  $implements['mymodule_a_view_override'] = array(
    'type' => 'view_override',
    'label' => t("Display an 'access denied' message"),
    // This would not be necessary, as it is the default when omitted.
    'override' => 'mymodule_a_view_override_override',
    'override_file' => 'override.inc',
    // This is also not necessary, as it is the default.
    'override_path' => drupal_get_path('module', 'mymodule'),
  );
}

/**
 * @} End of "addtogroup hooks".
 */
/**
 * @addtogroup callbacks
 * @{
 *
 */

/**
 * Example callback for a configure implementation.
 *
 * Actual function name can be whatever is defined
 * by the implementation in hook_fieldaccess_info,
 * and defaults to the access name suffixed with configure.
 * eg: mymodule_unique_name_view_configure().
 *
 * @param array $settings
 * The saved settings, if the field instance has already been configured.
 * @param array $form
 * The original form.
 * @param array $form_state
 * The original $form_state.
 *
 * @return array
 * Form elements that can be configured. The actual saving of these settings
 * are taken care of by Field Access API module.
 * Form validation and additional submission function will be automatically
 * devived from the form constructor name and called if they exist.
 * In this example, they would be :
 * - mymodule_unique_name_view_configure_validate()
 * - mymodule_unique_name_view_configure_submit()
 *
 */
function callback_fieldaccess_configure($settings, &$form, &$form_state) {
  $colors = array(
    'blue' => t('blue'),
    'red' => t('red'),
    'green' => t('green'),
  );
  $elements['shirt_color'] = array(
    '#type' => 'select',
    '#title' => t('Deny access to users that wear a shirt of this color.'),
    '#options' => $colors,
    '#default_value' => isset($settings['shirt_color']) ? $settings['shirt_color'] : 'blue',
  );
  return $elements;
}

/**
 * Example callback for a 'grant' access implementation.
 *
 * Actual function name can be whatever is defined
 * by the implementation in hook_fieldaccess_info,
 * and defaults to the access name suffixed with grant.
 * eg: mymodule_unique_name_view_grant().
 *
 * @param array $settings
 * An array of settings, as defined by the 'configure' callback
 * and configured on the field edit form.
 * @param oject $object
 * The entity object to which the field is attached to.
 * @param oject $account
 * The account object for the user that tries to access the field.
 * @param array $context
 * An array containing at least :
 * - the entity_type of the entity the field is attached to.
 * - the entity object to which the field is attached to.
 * - the original field array.
 *
 * @return bool
 * Weither the user is granted the access to the field.
 *
 */
function callback_fieldaccess_grant($settings, $object, $account, $context) {
  // Fetch the color from settings.
  $color = $settings['shirt_color'];
  // Check against dummy property.
  if ($account->shirt === $color) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Example callback for a 'override' access implementation.
 *
 * Actual function name can be whatever is defined
 * by the implementation in hook_fieldaccess_info,
 * and defaults to the access name suffixed with override.
 * eg: mymodule_a_view_override_override().
 *
 * @param array $settings
 * An array of settings, as defined by the 'configure' callback
 * and configured on the field edit form.
 * @param array $element
 * The original field $element array, before being rendered.
 * @param array $context
 * An array containing at least :
 * - the entity_type of the entity the field is attached to.
 * - the entity object to which the field is attached to.
 * - the original field array.
 *
 * @return array
 * A renderable array, that will be displayed instead of the
 * original field element.
 * 
 */
function callback_fieldaccess_override($settings, $element, $context) {
  $element = array(
    '#markup' => t('You need to change your shirt to access this story.'),
  );
  return $element;
}

/**
 * @} End of "addtogroup callbacks".
 */
