<?php

/**
 * @file
 * Defines a test setup for running tests on Field Access
 */
/*
 * Implements hook_init().
 */
function fa_test_init() {
  if (!variable_get('fa_node_processed', FALSE)) {
    $content_type = node_type_load('fa_node');
    fa_test_node_type_insert($content_type);
    variable_set('fa_node_processed', TRUE);
  }
}

/**
 * Implements hook_node_info().
 */
function fa_test_node_info() {
  return array(
    'fa_node' => array(
      'name' => t('Field Access node'),
      'base' => 'fa_node',
      'description' => t('Test node type for testing Field Access.'),
      'locked' => FALSE,
    )
  );
}

/**
 * Implement hook_form().
 */
function fa_test_form($node, $form_state) {
  return node_content_form($node, $form_state);
}

/**
 * Implements hook_node_type_insert().
 */
function fa_test_node_type_insert($content_type) {
  if ($content_type->type == 'fa_node') {
    $body_instance = node_add_body_field($content_type);
    // Save our changes to the body field instance.
    field_update_instance($body_instance);
    // Create all the fields we are adding to our content type.
    foreach (_fa_test_declare_fields() as $field) {
      if (!field_info_field($field['field_name'])) {
        field_create_field($field);
      }
    }
    // Create all the instances for our fields.
    foreach (_fa_test_declare_instances() as $instance) {
      if (!field_info_instance('node', $instance['field_name'], 'fa_node')) {
        $instance['entity_type'] = 'node';
        $instance['bundle'] = 'fa_node';
        field_create_instance($instance);
      }
    }
  }
}

/**
 * Implements hook_fieldaccess_info().
 */
function fa_test_fieldaccess_info() {
  return array(
    'fa_test_view' => array(
      'type' => 'view',
      'label' => t('Test field view access grant'),
    ),
    'fa_test_view_override' => array(
      'type' => 'view_override',
      'label' => t('Test field view access override'),
    ),
    'fa_test_edit' => array(
      'type' => 'edit',
      'label' => t('Test field edit access grant'),
    ),
    'fa_test_edit_override' => array(
      'type' => 'edit_override',
      'label' => t('Test field edit access override'),
    ),
  );
}

/**
 * Define the fields for our content type.
 *
 * @return
 *  An associative array specifying the fields we wish to add to our
 *  new node type.
 */
function _fa_test_declare_fields() {
  $fields = array();
  $cardinalties = array(
    1 => 'unique',
    3 => 'multiple',
    0 => 'unlimited',
  );
  foreach (field_info_field_types() as $type => $field) {

    foreach ($cardinalties as $cardinality => $cardinality_name) {
      $field_id = substr($type . '_' . $cardinality_name, 0, 32);
      $field_name = $field['label'] . ' ' . $cardinality_name;
      $fields[$field_id] = array(
        'field_name' => $field_id,
        'cardinality' => $cardinality,
        'type' => $type,
        'settings' => $field['settings'],
        'label' => $field_name,
      );
    }
  }
  return $fields;
}

/**
 * Define the field instances for our content type.
 */
function _fa_test_declare_instances() {
  $instances = _fa_test_declare_fields();
  foreach ($instances as $id => $instance) {
    unset($instances[$id]['type']);
    unset($instances[$id]['settings']);
  }

  return $instances;
}
