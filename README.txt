
 Field Access API module
-----------------
by bellesmanieres, pascal.morin@codeenigma.com

This module simplifies the creation of new access rules for Drupal fields.
This is an API module. You only need to enable it if a module depends on it or
you are a developer.

 Defining a new access setting
---------------------------------------
A new access can be implemented with hook_fieldaccess_info().
There are 4 types of possible implementations, that can be of 2 kind :
- Grant implementations ('view', 'edit') :
  These are to grant or deny access to the field, respectively when
  it is being viewed or edited.
- Override implementations ('view_override', 'edit_override') :
  These are to alter the fields rendering if access has been denied,
  instead of simply not displaying it.
See fieldaccess.api.inc for more details.

 Caches
---------------------------------------
Field Access uses its own cache bin to store field access settings, called
'cache_fieldaccess'.

For users of the 'admin_menu' module, a menu entry is provided within the
"Flush all caches" option, to flush only the fieldaccess cache.

For other users, the 'cache_fieldaccess' table can be flushed in the same way
as the general drupal caches.